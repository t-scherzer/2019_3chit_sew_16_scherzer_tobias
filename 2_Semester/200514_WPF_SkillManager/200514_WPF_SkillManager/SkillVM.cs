﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200514_WPF_SkillManager
{
    class SkillVM : INotifyPropertyChanged
    {
        public Skill skill;
        public Skill Skill
        {
            get { return skill; }
            set
            {
                skill = value;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        private Skill selectedSkill = null;
        public Skill SelectedSkill
        {
            get { return selectedSkill; }
            set { selectedSkill = value; OnPropertyChanged("SelectedSkill"); OnPropertyChanged("Skills"); }
        }
        private ObservableCollection<Skill> skills = new ObservableCollection<Skill>();
        public ObservableCollection<Skill> Skills
        {
            get { return skills; }
            set { skills = value; }
        }
        public SkillVM()
        {
            AddSkillCommand = new AddSkillCommand(this);
            AddPercentageCommand = new AddPercentageCommand(this);
            RemovePercentageCommand = new RemovePercentageCommand(this);
            Skill = new Skill();
            Skills = new ObservableCollection<Skill> { new Skill(30, "SEW"), new Skill(80, "MedT"), new Skill(90, "AM") };
        }
        public SkillVM(ObservableCollection<Skill> skills) : this()
        {
            Skills = skills;
        }
        public ICommand AddSkillCommand { get; set; }
        public ICommand AddPercentageCommand { get; set; }
        public ICommand RemovePercentageCommand { get; set; }
    }
}
