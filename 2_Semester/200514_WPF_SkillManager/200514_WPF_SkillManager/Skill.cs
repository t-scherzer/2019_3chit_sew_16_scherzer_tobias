﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200514_WPF_SkillManager
{
    class Skill : INotifyPropertyChanged
    {
        private int percent;
        public int Percent { get{ return percent; } set{ percent = value; OnPropertyChanged("Percent"); } }
        public string Name { get; set; }
        public Skill(int percent, string name) { this.percent = percent; Name = name; }
        public Skill() { percent = 0; Name = ""; }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
