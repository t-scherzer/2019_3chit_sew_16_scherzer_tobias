﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200514_WPF_SkillManager
{
    class RemovePercentageCommand : ICommand
    {
        SkillVM parent;
        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter) { return true; }
        public void Execute(object parameter)
        {
            parent.SelectedSkill.Percent -= 10;
            //parent.Skills[parent.Skills.IndexOf(parent.SelectedSkill)].Percent -= 10;
            parent.OnPropertyChanged("Skills");
            parent.OnPropertyChanged("SelectedSkill");
        }
        public RemovePercentageCommand(SkillVM parent) { this.parent = parent; }
    }
}
