﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200514_WPF_SkillManager
{
    class AddSkillCommand : ICommand
    {
        SkillVM parent;
        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter) { return true; }
        public void Execute(object parameter)
        {
            //parent.Skill = new Skill(0, parent.Skill.Name);
            parent.Skills.Add(new Skill(0, parent.Skill.Name));
            parent.OnPropertyChanged("Skills");
            parent.OnPropertyChanged("Percent");
            parent.OnPropertyChanged("SelectedSkill");
        }
        public AddSkillCommand(SkillVM parent) { this.parent = parent; }
    }
}
