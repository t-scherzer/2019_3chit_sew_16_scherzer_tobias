﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace _200423_WPF_HelloBoundWorld
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new TestObject();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BindingExpression expr = Explicit.GetBindingExpression(TextBox.TextProperty);
            expr.UpdateSource();
        }
    }
    public class TestObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string _text = "Hello World";

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                OnPropertyChanged("Text");
            }
        }
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
