﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace _200310_JumpingBall
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            timer.Tick += Physics;
            timer.Interval = TimeSpan.FromSeconds(0.05);
            timer.Start();
            
        }
        int score = 0;
        DispatcherTimer timer = new DispatcherTimer();
        bool goingDown = true, goingRight = true, goingDown1 = true, goingRight1 = true, goingDown2 = true, goingRight2 = true;
        private void Physics(object sender, EventArgs e)
        {
            double speed = 3.0;
            if (FastCheck.IsChecked.Value) speed = 10.0;
            Move(speed);
        }
        private void Move(double speed)
        {
            double x = Canvas.GetLeft(Ball), y = Canvas.GetTop(Ball);
            if (goingRight) x += speed;
            else x -= speed;
            if (goingDown) y += speed;
            else y -= speed;
            if (x + Ball.Width > BallCanvas.ActualWidth)
            {
                goingRight = false;
                x = BallCanvas.ActualWidth - Ball.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0)
            {
                goingRight = true;
                x = 0;
                System.Media.SystemSounds.Asterisk.Play();
            }
            Canvas.SetLeft(Ball, x);
            if (y + Ball.Height > BallCanvas.ActualHeight)
            {
                goingDown = false;
                y = BallCanvas.ActualHeight - Ball.Height;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (y < 0)
            {
                goingDown = true;
                y = 0;
                System.Media.SystemSounds.Asterisk.Play();
            }
            Canvas.SetTop(Ball, y);

            x = Canvas.GetLeft(Ball1); y = Canvas.GetTop(Ball1);
            if (goingRight1) x += speed;
            else x -= speed;
            if (goingDown1) y += speed;
            else y -= speed;
            if (x + Ball1.Width > BallCanvas.ActualWidth)
            {
                goingRight1 = false;
                x = BallCanvas.ActualWidth - Ball1.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0)
            {
                goingRight1 = true;
                x = 0;
                System.Media.SystemSounds.Asterisk.Play();
            }
            Canvas.SetLeft(Ball1, x);
            if (y + Ball1.Height > BallCanvas.ActualHeight)
            {
                goingDown1 = false;
                y = BallCanvas.ActualHeight - Ball1.Height;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (y < 0)
            {
                goingDown1 = true;
                y = 0;
                System.Media.SystemSounds.Asterisk.Play();
            }
            Canvas.SetTop(Ball1, y);

            x = Canvas.GetLeft(Ball2); y = Canvas.GetTop(Ball2);
            if (goingRight2) x += speed;
            else x -= speed;
            if (goingDown2) y += speed;
            else y -= speed;
            if (x + Ball2.Width > BallCanvas.ActualWidth)
            {
                goingRight2 = false;
                x = BallCanvas.ActualWidth - Ball2.Width;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (x < 0)
            {
                goingRight2 = true;
                x = 0;
                System.Media.SystemSounds.Asterisk.Play();
            }
            Canvas.SetLeft(Ball2, x);
            if (y + Ball2.Height > BallCanvas.ActualHeight)
            {
                goingDown2 = false;
                y = BallCanvas.ActualHeight - Ball2.Height;
                System.Media.SystemSounds.Asterisk.Play();
            }
            else if (y < 0)
            {
                goingDown2 = true;
                y = 0;
                System.Media.SystemSounds.Asterisk.Play();
            }
            Canvas.SetTop(Ball2, y);
            
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            timer.IsEnabled = !timer.IsEnabled;
        }

        private void Ball_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Score.Text = "Score: " + ++score;
        }

        private void RedRadio_Checked(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Red;
        }

        private void BlueRadio_Checked(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Blue;
        }

        private void GreenRadio_Checked(object sender, RoutedEventArgs e)
        {
            Ball.Fill = Brushes.Green;
        }
    }
}
