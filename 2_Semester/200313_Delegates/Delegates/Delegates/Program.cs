﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberChanger unnoetigeSchreibweiseEinerKlassenMethode1 = new NumberChanger(DelegateAppl.TestDelegate.AddNum);
            NumberChanger unnoetigeSchreibweiseEinerKlassenMethode2 = new NumberChanger(DelegateAppl.TestDelegate.MultNum);
            unnoetigeSchreibweiseEinerKlassenMethode1(25);
            Console.WriteLine(DelegateAppl.TestDelegate.getNum());
            unnoetigeSchreibweiseEinerKlassenMethode2(5);
            Console.WriteLine(DelegateAppl.TestDelegate.getNum());
            Console.ReadLine(); //ich bin begeistert, sowoas hätte man niemals in einer class machen können :O DANKE, DELEGATES. IHR RETTET UNSER LEBEN
            NumberChanger unnoetigeSchreibweiseEinerKlassenMethode;
            unnoetigeSchreibweiseEinerKlassenMethode = unnoetigeSchreibweiseEinerKlassenMethode1;
            unnoetigeSchreibweiseEinerKlassenMethode += unnoetigeSchreibweiseEinerKlassenMethode2;
            unnoetigeSchreibweiseEinerKlassenMethode(5);
            Console.WriteLine(DelegateAppl.TestDelegate.getNum());
            
        }
    }
}
delegate int NumberChanger(int n);
namespace DelegateAppl
{
    class TestDelegate //wieso existiert delegates überhaupt? sind die leute zu faul eine Klasse zu definieren (obwohl sie das eh müssen????????) 
    {
        static int num = 10;
        public static int AddNum(int p)
        {
            num += p;
            return num;
        }
        public static int MultNum(int q)
        {
            num *= q;
            return num;
        }
        public static int getNum()
        {
            return num;
        }
    }
    class Employee { public int id; public string name; public decimal salary; public DateTime PositionChangeDate;}
    class Company
    {
        private void PositionDateUpdate(Employee employee) { employee.PositionChangeDate = DateTime.Now; }
        private void EmployeeSalaryRaise(Employee employee) { employee.salary *= (decimal)0.1; }
        public List<Employee> Employees { get; }
        public delegate void CustomDel(Employee employee);
        CustomDel SalaryRaiseDel, PositionDateDel, EmployeePromoteMulticastDelegate;
        public Company()
        {
            Employees = new List<Employee>()
            {
                new Employee(){id = 1, name ="Max", salary=10},
                new Employee(){id = 2, name ="John", salary=20},
                new Employee(){id = 3, name ="Kurt", salary=15},
                new Employee(){id = 4, name ="Nik", salary=25}
            };
            SalaryRaiseDel = new CustomDel(EmployeeSalaryRaise);
            PositionDateDel = new CustomDel(PositionDateUpdate);
            EmployeePromoteMulticastDelegate = PositionDateDel + SalaryRaiseDel;
        }
        
    }
}
//Bücherladen nicht im Skript "EventsDelegatesExercises" enthalten

