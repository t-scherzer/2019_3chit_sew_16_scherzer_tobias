﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Events
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
public delegate string BirthdayDelegate(string str);
class Person
{
    event BirthdayDelegate SendWishes;
    public Person()
    {
        this.SendWishes += new BirthdayDelegate(this.Congrats);
        this.SendWishes += new BirthdayDelegate(this.Congrats2);
    }
    public string Congrats(string nickname)
    {
        return "Happy Birthday " + nickname;
    }
    public string Congrats2(string nickname)
    {
        return "Have a wonderful day";
    }
    public static void Test()
    {
        Person p = new Person();
        string result = p.SendWishes("Sweety");
        Console.WriteLine(result);
    }
}
public class Metronome
{
    public delegate void TickHandler(Metronome m, EventArgs e);
    public EventArgs e = null;
    public event TickHandler Tick;
    public void Start()
    {
        while(true)
        {
            System.Threading.Thread.Sleep(3000);
            if(Tick != null)
            {
                Tick(this, e);
            }
        }
    }
}
public class Passenger
{
    public string name;
    public int weight;
    public Passenger(string name, int weight)
    {
        this.name = name;
        this.weight = weight;
    }
}
public class Elevator
{
    int currentweight = 0, maxWeight;
    string label;
    List<Passenger> passengers = new List<Passenger>();
    //keine Ahnung wo genau man hier einen Umweg machen soll, um ein Event zu brauchen???
    //im Skript steht auch nicht genau was das event sein soll
    public delegate void BoardingHandler(Elevator ele);
    public event BoardingHandler Boarder;
    public Elevator(string label, int maxWeight)
    {
        this.label = label;
        this.maxWeight = maxWeight;
        //Boarder += new BoardingHandler(CheckWeight);
    }
    public void Boarding(Passenger p)
    {
        currentweight += p.weight;
        passengers.Add(p);
        CheckWeight(p);
    }
    private void CheckWeight(Passenger p)
    {
        if (currentweight > maxWeight) AlarmTone(p);
    }
    public void AlarmTone(Passenger p)
    {
        Console.WriteLine(p.name + " hat den Lift überladen");
        Console.WriteLine("Gewicht: " + currentweight);
    }
    private void FindLeastEfficientPerson()
    {
        int overWeightAmount = currentweight - maxWeight;
        List<Passenger> list_passengers = new List<Passenger>();
        passengers.CopyTo(list_passengers.ToArray());
        list_passengers.Sort();
        for (int i = 0; i < list_passengers.Count; i++)
        {
            if(list_passengers[i].weight > overWeightAmount)
                Console.WriteLine(list_passengers[i].name + " muss den Lift verlassen");
        }
    }
    public void Unboarding(Passenger p)
    {
        currentweight -= p.weight;
        passengers.Remove(p);
    }
    public static void TestElevator()
    {
        Elevator ele = new Elevator("label", 500);
        for (int i = 0; i < 7; i++)
        {
            int weight = 50 + i * 10;
            string name = "Max" + 1;
            Passenger p = new Passenger(name, weight);
            ele.Boarding(p);
        }
    }
}
public class School
{
    public delegate void FireDelegate(string type);
    public event FireDelegate FireAlarm;
    public School()
    {
        
    }
    public int GetPlaceNumber(Clazz c)
    {
        return 1;
    }
    public void StartFireAlarm(string type)
    {
        FireAlarm("Real, einmal hin, alles drin");
    }
}
public class Clazz
{
    public string name;
    public Clazz(string name)
    {
        this.name = name;
    }
}
public class Pupil
{
    private string name;
    public Clazz clazz;
    private School school;
    public Pupil(string name, School school, Clazz clazz)
    {
        this.name = name;
        this.school = school;
        this.clazz = clazz;
        school.FireAlarm += new School.FireDelegate(EscapeRoute);
    }
    private void EscapeRoute(string type)
    {
        Console.WriteLine(name + " begibt sich auf Hof " + school.GetPlaceNumber(clazz));
    }
}
