﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace _200615_TimeToolWPF
{
    public class WindowAddVM : INotifyPropertyChanged
    {
        public RelayCommand EndCommand { get; set; } = new RelayCommand(o => (((o as Button).Parent as Grid).Parent as Window).Close(), o => true);
        public _200601_TimeToolCSVLogic.Task task = new _200601_TimeToolCSVLogic.Task("", "", false, false, _200601_TimeToolCSVLogic.EState.OPEN, DateTime.Now);
        public string Name
        {
            get { return task.Name; }
            set { task.Name = value; OnPropertyChnaged("Name"); }
        }
        public string Description
        {
            get { return task.Description; }
            set { task.Description = value; OnPropertyChnaged("Description"); }
        }
        public bool Urgent
        {
            get { return task.Urgent; }
            set { task.Urgent = value; OnPropertyChnaged("Urgent"); }
        }
        public bool Important
        {
            get { return task.Important; }
            set { task.Important = value; OnPropertyChnaged("Important"); }
        }
        public _200601_TimeToolCSVLogic.EState State
        {
            get { return task.State; }
            set { task.State = value; OnPropertyChnaged("State"); }
        }
        public DateTime DoneTill
        {
            get { return task.DoneTill; }
            set { task.DoneTill = value; OnPropertyChnaged("DoneTill"); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChnaged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
