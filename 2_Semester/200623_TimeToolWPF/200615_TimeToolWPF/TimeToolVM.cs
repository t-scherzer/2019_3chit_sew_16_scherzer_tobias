﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace _200615_TimeToolWPF
{
    public class TimeToolVM : INotifyPropertyChanged
    {
        private _200601_TimeToolCSVLogic.AllTasks allTasks = new _200601_TimeToolCSVLogic.AllTasks();
        public _200601_TimeToolCSVLogic.AllTasks AllTasks
        {
            get { return allTasks; }
            set { allTasks = value; OnPropertyChnaged("AllTasks"); }
        }
        private _200601_TimeToolCSVLogic.Task selTask;
        public _200601_TimeToolCSVLogic.Task SelectedTask
        {
            get { return selTask; }
            set { selTask = value; OnPropertyChnaged("SelectedTask"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChnaged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public RelayCommand AddCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }
        public RelayCommand ChangeCommand { get; set; }
        public TimeToolVM()
        {
            AddCommand = new RelayCommand(o => AddComm(o), o => true);
            DeleteCommand = new RelayCommand(o => DelComm(o), o => SelectedTask != null);
            ChangeCommand = new RelayCommand(o => CngComm(o), o => SelectedTask != null);
        }
        private void CngComm(object o)
        {
            WindowAdd w = new WindowAdd();
            w.NameTextBox.Text = SelectedTask.Name;
            w.DescriptionTextBox.Text = SelectedTask.Description;
            w.UrgentCheckBox.IsChecked = SelectedTask.Urgent;
            w.ImportantCheckBox.IsChecked = SelectedTask.Important;
            w.StateComboBox.SelectedValue = SelectedTask.State;
            w.DatePickerBox.SelectedDate = SelectedTask.DoneTill;
            allTasks.DeleteTask(SelectedTask.Important, SelectedTask.Urgent, SelectedTask);
            _200601_TimeToolCSVLogic.Task t = new _200601_TimeToolCSVLogic.Task("", "", false, false, _200601_TimeToolCSVLogic.EState.OPEN, DateTime.Now);
            if (w.ShowDialog() == false)
            {
                t.Name = w.NameTextBox.Text;
                t.Description = w.DescriptionTextBox.Text;
                t.Urgent = (bool)w.UrgentCheckBox.IsChecked;
                t.Important = (bool)w.ImportantCheckBox.IsChecked;
                t.State = (_200601_TimeToolCSVLogic.EState)w.StateComboBox.SelectedValue;
                t.DoneTill = (DateTime)w.DatePickerBox.SelectedDate;
            }
            allTasks.AddTask(t, t.Urgent, t.Important);
        }
        private void DelComm(object o)
        {
            allTasks.DeleteTask(SelectedTask.Important, SelectedTask.Urgent, SelectedTask);
        }
        private void AddComm(object o)
        {
            _200601_TimeToolCSVLogic.Task t = new _200601_TimeToolCSVLogic.Task("", "", false, false, _200601_TimeToolCSVLogic.EState.OPEN, DateTime.Now);
            WindowAdd w = new WindowAdd();
            if(w.ShowDialog() == false)
            {
                t.Name = w.NameTextBox.Text;
                t.Description = w.DescriptionTextBox.Text;
                t.Urgent = (bool)w.UrgentCheckBox.IsChecked;
                t.Important = (bool)w.ImportantCheckBox.IsChecked;
                t.State = (_200601_TimeToolCSVLogic.EState)w.StateComboBox.SelectedValue;
                t.DoneTill = (DateTime)w.DatePickerBox.SelectedDate;
            }
            allTasks.AddTask(t, t.Urgent, t.Important);
        }
    }
}
