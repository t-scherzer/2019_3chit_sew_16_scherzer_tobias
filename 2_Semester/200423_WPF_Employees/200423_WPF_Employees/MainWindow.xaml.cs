﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace _200423_WPF_Employees
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public class Employee
    {
        public int MemberID { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Salary { get; set; }
    }
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            this.DataContext = this;
            InitializeComponent(); 
        }
    }
    class MainViewModelVMVMVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Employee> Employees
        {
            get { return employees; }
            set { employees = value; }
        }
        protected virtual void OnPropetyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        private ObservableCollection<Employee> employees = new ObservableCollection<Employee>();
        public MainViewModelVMVMVM()
        {
            Employee employee = new Employee()
            {
                MemberID = 6,
                Name = "Jane Doe",
                Department = "Banking",
                Phone = "31234748",
                Email = "Jane.Doe@Company.Com",
                Salary = "3350"
            };
            employees.Add(employee);
            employees.Add(new Employee
            {
                MemberID = 1,
                Name = "John Hancock",
                Department = "IT",
                Phone = "31234743",
                Email = @"John.Hancock@Company.com",
                Salary = "3450.44"
            });
            employees.Add(new Employee
            {
                MemberID = 2,
                Name = "Jane Hayes",
                Department = "Sales",
                Phone = "31234744",
                Email = @"Jane.Hayes@Company.com",
                Salary = "3700"
            });
            employees.Add(new Employee
            {
                MemberID = 3,
                Name = "Larry Jones",
                Department = "Marketing",
                Phone = "31234745",
                Email = @"Larry.Jones@Company.com",
                Salary = "3000"
            });
            employees.Add(new Employee
            {
                MemberID = 4,
                Name = "Patricia Palce",
                Department = "Secretary",
                Phone = "31234746",
                Email = @"Patricia.Palce@Company.com",
                Salary = "2900"
            });
            employees.Add(new Employee
            {
                MemberID = 5,
                Name = "Jean L. Trickard",
                Department = "Director",
                Phone = "31234747",
                Email = @"Jean.L.Tricard@Company.com",
                Salary = "5400"
            });
        }
    }
}
