﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200514_WPF_ListOfNames
{
    class AddNameCommand : ICommand
    {
        Model parent;
        public AddNameCommand(Model parent)
        {
            this.parent = parent;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter)
        {
            parent.AddedNames.Add(parent.CurrentName);
            parent.CurrentName = null;
        }
    }
}
