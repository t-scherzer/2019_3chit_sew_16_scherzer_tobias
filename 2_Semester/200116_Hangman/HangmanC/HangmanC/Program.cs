﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanC
{
    class Program
    {
        static void Main(string[] args)
        {
            View view = new View();
            while(true)
            {
                try
                {
                    view.Play(Convert.ToChar(Console.ReadLine()));
                }
                catch { }
            }
        }
    }
}
