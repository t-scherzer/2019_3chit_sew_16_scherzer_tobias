﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HangmanLogic;

namespace HangmanC
{
    class View
    {
        int amountLives = 9;
        string word, guessingString;
        bool lastHit = false;
        Logic logic;
        static private string FillString(string word)
        {
            StringBuilder temp = new StringBuilder();
            for (int i = 0; i < word.Length; i++)
            {
                temp.Append('_');
            }
            return temp.ToString();
        }
        public void Play(char letter)
        {
            Console.Clear();
            if (amountLives != 0 && letter != null && guessingString != word)
            {
                logic.Operate(ref word, ref guessingString, ref lastHit, letter, ref amountLives);
                Console.WriteLine(guessingString);
                RefreshView();
            }
        }
        private void RefreshView()
        {
            if (amountLives == 0) Console.WriteLine("Game Over");
            switch (amountLives)
            {
                case 8:
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;
                case 7:
                    Console.WriteLine("   |");
                    Console.WriteLine("   |  ");
                    Console.WriteLine("   |  ");
                    Console.WriteLine("   |  ");
                    Console.WriteLine("   |  ");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;
                case 6:
                    Console.WriteLine("   _________");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;
                case 5:
                    Console.WriteLine("   _________");
                    Console.WriteLine("   |/");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;
                case 4:
                    Console.WriteLine("   _________");
                    Console.WriteLine("   |/  |");
                    Console.WriteLine("   |   O");
                    Console.WriteLine("   |   ");
                    Console.WriteLine("   |   ");
                    Console.WriteLine("   |   ");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;
                case 3:
                    Console.WriteLine("   _________");
                    Console.WriteLine("   |/  |");
                    Console.WriteLine("   |   O");
                    Console.WriteLine("   |   | ");
                    Console.WriteLine("   |   |");
                    Console.WriteLine("   |     ");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;
                case 2:
                    Console.WriteLine("   _________");
                    Console.WriteLine("   |/  |");
                    Console.WriteLine("   |   O");
                    Console.WriteLine("   |  (| ");
                    Console.WriteLine("   |   |");
                    Console.WriteLine("   |     ");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;
                case 1:
                    Console.WriteLine("   _________");
                    Console.WriteLine("   |/  |");
                    Console.WriteLine("   |   O");
                    Console.WriteLine("   |  (|) ");
                    Console.WriteLine("   |   |");
                    Console.WriteLine("   |     ");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\"); break;

                case 0:
                    Console.WriteLine("   _________");
                    Console.WriteLine("   |/  |");
                    Console.WriteLine("   |   O");
                    Console.WriteLine("   |  (|) ");
                    Console.WriteLine("   |   |");
                    Console.WriteLine("   |   (\\  ");
                    Console.WriteLine("   |");
                    Console.WriteLine("  ---------");
                    Console.WriteLine(" /         \\");
                    Console.WriteLine("/           \\");
                    Console.WriteLine(); break;
            }
            if (guessingString == word) Console.WriteLine("Stickman saved");
        }
        public View()
        {
            word = Logic.GetRandomWord();
            logic = new Logic(word, amountLives);
            guessingString = FillString(word);
            Console.WriteLine(guessingString);
        }
    }
}
