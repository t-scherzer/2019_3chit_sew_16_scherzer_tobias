﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using HangmanLogic;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HangmanWPF
{
    class View
    {
        MainWindow f1;
        int amountLives = 10, originalLives = 10;
        string word, guessingString;
        bool lastHit = false;
        object txtWord, txtInput, pBox;
        Logic logic;
        static private string FillString(string word)
        {
            StringBuilder temp = new StringBuilder();
            for (int i = 0; i < word.Length; i++)
            {
                temp.Append('_');
            }
            return temp.ToString();
        }
        public void Play()
        {
            if (amountLives != 0 && (txtInput as TextBox).Text != "" && guessingString != word)
            {
                logic.Operate(ref word, ref guessingString, ref lastHit, Convert.ToChar((txtInput as TextBox).Text[0]), ref amountLives);
                (txtWord as TextBlock).Text = guessingString;
                (txtInput as TextBox).Text = "";
                RefreshView();
            }
        }
        private void RefreshView()
        {
            if (amountLives == 0) f1.Background = Brushes.Red;
            (pBox as Image).Source = new BitmapImage(new Uri("../../../../hangman/" + (originalLives - amountLives) + ".jpg"));
            if (guessingString == word) f1.Background = Brushes.Green;
        }
        public View(MainWindow f1)
        {
            var a = f1.FindName("pBox");
            this.f1 = f1;
            word = Logic.GetRandomWord();
            logic = new Logic(word, amountLives);
            pBox = f1.FindName("pBox");
            txtWord = f1.FindName("txtWord");
            txtInput = f1.FindName("txtInput");
            guessingString = FillString(word);
            (txtWord as TextBlock).Text = guessingString;
            //(txtWord as TextBlock).UpdateLayout();
        }
    }
}
