﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HangmanLogic;
using System.IO;
using System.Drawing;

namespace HangmanWF
{
    class View
    {
        Form1 f1;
        int amountLives = 10, originalLives = 10;
        string word, guessingString;
        bool lastHit = false;
        Control lblWord, txtInput, pBox;
        Logic logic;
        static private string FillString(string word)
        {
            StringBuilder temp = new StringBuilder();
            for (int i = 0; i < word.Length; i++)
            {
                temp.Append('_');
            }
            return temp.ToString();
        }
        public void Play()
        {
            if(amountLives != 0 && txtInput.Text != "" && guessingString != word)
            {
                logic.Operate(ref word, ref guessingString, ref lastHit, Convert.ToChar(txtInput.Text[0]), ref amountLives);
                lblWord.Text = guessingString;
                txtInput.Text = "";
                RefreshView();
            }
        }
        private void RefreshView()
        {
            if (amountLives == 0) f1.BackColor = Color.Red;
            (pBox as PictureBox).Image = Image.FromFile("../../../../hangman/" + (originalLives - amountLives) + ".jpg");
            if (guessingString == word) f1.BackColor = Color.Green;
        }
        public View(Form1 f1)
        {
            this.f1 = f1;
            word = Logic.GetRandomWord();
            logic = new Logic(word, amountLives);
            pBox = f1.Controls.Find("pBox", true)[0];
            lblWord = f1.Controls.Find("lblWord", true)[0];
            txtInput = f1.Controls.Find("txtWord", true)[0];
            guessingString = FillString(word);
            lblWord.Text = guessingString;
        }
    }
}
