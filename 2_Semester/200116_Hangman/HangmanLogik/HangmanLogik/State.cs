﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanLogic
{
    interface IState
    {
        int AmountLives { get; set; }
        string Word { get; set; }
        void Work(ref string word, ref string guessingString, ref bool lastHit, char letter, ref int amountLives);
        //bool CheckInput(char letter);
        //string GetWord();
    }
}
