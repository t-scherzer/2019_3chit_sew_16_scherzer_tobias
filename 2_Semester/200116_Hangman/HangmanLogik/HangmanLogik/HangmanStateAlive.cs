﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanLogic
{
    class HangmanStateAlive : IState
    {
        private int amountLives;
        private string word;
        private StringBuilder guessingString = new StringBuilder();
        public int AmountLives
        {
            get { return amountLives; }
            set { amountLives = value; }
        }
        public string Word
        {
            get { return word; }
            set { word = value; }
        }
        public string GetWord()
        {
            return Word;
        }
        public bool CheckInput(char letter, ref string guessingString, ref int remainingLives, ref bool lastHit)
        {
            bool found = false;
            foreach (char let in Word)
            {
                if (let.ToString().ToLower() == letter.ToString().ToLower()) { found = true; break; }
            }
            if(found)
            {
                lastHit = true;
                for (int i = 0; i < word.Length; i++)
                {
                    if(letter.ToString().ToLower() == Word[i].ToString().ToLower())
                    {
                        this.guessingString[i] = Word[i];
                    }
                }
                guessingString = this.guessingString.ToString();
            }
            else
            {
                AmountLives--;
                remainingLives--;
            }
            return found;
        }
        public void Work(ref string word, ref string guessingString, ref bool lastHit, char letter, ref int amountRemainingLives)
        {
            CheckInput(letter, ref guessingString, ref amountRemainingLives, ref lastHit);
        }
        public HangmanStateAlive(string word, int amountLives)
        {
            AmountLives = amountLives;
            Word = word;
            for (int i = 0; i < word.Length; i++)
            {
                guessingString.Append('_');
            }
        }
    }
}
