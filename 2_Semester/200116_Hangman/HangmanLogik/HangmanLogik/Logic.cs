﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HangmanLogic
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string msg) : base(msg) { }
    }
    public class Logic
    {
        static bool isCreated = false;
        private IState state;
        private bool gameover = false;
        public void Operate(ref string word, ref string guessingString, ref bool lastHit, char letter, ref int remainingLives)
        {
            state.Work(ref word, ref guessingString, ref lastHit, letter, ref remainingLives);
            gameover = (word == guessingString);
            ChangeState(word, remainingLives);
        }
        static public string GetRandomWord()
        {
            try
            {
                FileStream fs;
                try
                {
                    fs = new FileStream("words.txt", FileMode.Open);
                }
                catch { throw new NotFoundException("Nicht gefunden"); }
                StreamReader sr = new StreamReader(fs);
                Random random = new Random();
                string[] temp = sr.ReadToEnd().Split(';');
                sr.Close();
                fs.Close();
                return temp[random.Next(0, temp.Length)];
            }
            catch (NotFoundException e)
            {
                return "Hangman";
            }
        }
        private void ChangeState(string word, int remainingLives)
        {
            if(gameover)
            {
                if (remainingLives == 0) state = new HangmanStateDead(word, 0);
                else state = new HangmanStateSaved(word, 0);
            }
        }
        public Logic(string word, int amountLives)
        {
            if(!isCreated)
            {
                state = new HangmanStateAlive(word, amountLives);
                isCreated = true;
            }
        }
    }
}
