﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HangmanLogic
{
    class HangmanStateDead : IState
    {
        private int amountLives;
        private string word;
        public int AmountLives
        {
            get { return amountLives; }
            set { amountLives = value; }
        }
        public string Word
        {
            get { return word; }
            set { word = value; }
        }
        public void Work(ref string word, ref string guessingString, ref bool lastHit, char letter, ref int amountRemainingLives)
        {
            amountRemainingLives = -1;
        }
        public HangmanStateDead(string word, int amountLives)
        {
            AmountLives = amountLives;
            Word = word;
        }
    }
}
