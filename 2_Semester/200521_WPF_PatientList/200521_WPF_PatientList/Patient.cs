﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200521_WPF_PatientList
{
    class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Int64 MobileNumber { get; set; }
    }
}
