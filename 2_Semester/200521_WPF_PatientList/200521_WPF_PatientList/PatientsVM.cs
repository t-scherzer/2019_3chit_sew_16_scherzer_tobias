﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200521_WPF_PatientList
{
    class PatientsVM : INotifyPropertyChanged
    {
        public ICommand AddCommand { get; set; }
        public ICommand RemoveCommand { get; set; }
        public ICommand SearchCommand { get; set; }
        public PatientRepo Repo = new PatientRepo();
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public ObservableCollection<Patient> Patients
        {
            get { return Repo.Patients; }
            set { Repo.Patients = value; }
        }
        private Patient patient = new Patient();
        public Patient SelectedPatient
        {
            get { return patient; }
            set { patient = value; OnPropertyChanged("SelectedPatient"); }
        }
        public Patient CreatingPatient
        {
            get; set;
        } = new Patient();
        public PatientsVM()
        {
            AddCommand = new AddCommand(this);
            RemoveCommand = new RemoveCommand(this);
            SearchCommand = new SearchCommand(this);
        }
    }
}
