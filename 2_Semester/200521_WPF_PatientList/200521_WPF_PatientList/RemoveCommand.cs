﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200521_WPF_PatientList
{
    class RemoveCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        PatientsVM parent;
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            try { parent.Repo.Remove(parent.SelectedPatient.Id); }
            catch (Exception e) { }
            parent.OnPropertyChanged("Patients");
        }
        public RemoveCommand(PatientsVM parent)
        {
            this.parent = parent;
        }
    }
}
