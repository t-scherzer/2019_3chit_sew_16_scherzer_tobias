﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200521_WPF_PatientList
{
    class AddCommand : ICommand
    {
        PatientsVM parent;
        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter) { return true; }
        public void Execute(object parameter)
        {
            parent.Repo.Add(new Patient { Id = parent.CreatingPatient.Id, MobileNumber = parent.CreatingPatient.MobileNumber, Name = parent.CreatingPatient.Name });
            parent.OnPropertyChanged("Patients");
        }
        public AddCommand(PatientsVM parent)
        {
            this.parent = parent;
        }
    }
}
