﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200521_WPF_PatientList
{
    class PatientRepo : INotifyPropertyChanged
    {
        private ObservableCollection<Patient> patients = new ObservableCollection<Patient>();
        public ObservableCollection<Patient> Patients
        {
            get { return patients; }
            set { patients = value; OnPropertyChanged("Patients"); }
        }
        public void Add(Patient patient)
        {
            Patients.Add(patient);
        }
        public void Remove(int id)
        {
            Patients.Remove(Patients.First(x => x.Id == id));
        }
        public Patient Search(int id)
        {
            try { return Patients.First(x => x.Id == id); }
            catch(Exception e) { }
            return null;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
