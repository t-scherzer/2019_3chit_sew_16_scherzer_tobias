﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200521_WPF_PatientList
{
    class SearchCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        PatientsVM parent;
        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            parent.SelectedPatient = parent.Repo.Search(parent.CreatingPatient.Id);
            parent.OnPropertyChanged("Patients");
        }
        public SearchCommand(PatientsVM parent)
        {
            this.parent = parent;
        }
    }
}
