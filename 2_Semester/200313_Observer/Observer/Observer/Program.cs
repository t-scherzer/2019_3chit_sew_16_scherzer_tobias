﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            FAZVerlag verlag = new FAZVerlag();
            verlag.aboHinzufuegen(new FamilieFischer());
            verlag.aboHinzufuegen(new FamilieMeier());
            FirmaXY firma = new FirmaXY();
            verlag.aboHinzufuegen(firma);

            verlag.setAktuelleZeitung(new Zeitung("Skandal!"));
            //Familie Fischer erhielt die aktuelle Zeitung: Skandal! 
            //Familie Meier erhielt die aktuelle Zeitung: Skandal! 
            //Firma XY erhielt die aktuelle Zeitung: Skandal! 

            verlag.aboEntfernen(firma);
            verlag.setAktuelleZeitung(new Zeitung("Doch alles halb so wild!"));
            //Familie Fischer erhielt die aktuelle Zeitung: Doch alles halb so wild! 
            //Familie Meier erhielt die aktuelle Zeitung: Doch alles halb so wild!
            Console.ReadLine();
        }
    }
    
    public class FAZVerlag
    {

        private List<Abonnent> abonnentenList = new List<Abonnent>();

        private Zeitung aktuelleZeitung;


        public void aboHinzufuegen(Abonnent abonnent)
        {
            abonnentenList.Add(abonnent);
        }

        public void aboEntfernen(Abonnent abonnent)
        {
            abonnentenList.Remove(abonnent);
        }

        private void verteileZeitung(Zeitung zeitung)
        {
            foreach (Abonnent abonnent in abonnentenList)
            {
                abonnent.erhalteZeitung(zeitung);
            }
        }

        public void setAktuelleZeitung(Zeitung aktuelleZeitung)
        {
            this.aktuelleZeitung = aktuelleZeitung;
            //Nach dem einen neue Zeitung gesetzt wurde, werden alle Abonnenten benachrichtigt. 
            verteileZeitung(aktuelleZeitung);
        }

        public Zeitung getAktuelleZeitung()
        {
            return aktuelleZeitung;
        }
    }
    public abstract class Verlag
    {

        private List<Abonnent> abonnentenList = new List<Abonnent>();

        public void aboHinzufuegen(Abonnent abonnent)
        {
            abonnentenList.Add(abonnent);
        }

        public void aboEntfernen(Abonnent abonnent)
        {
            abonnentenList.Remove(abonnent);
        }

        protected void verteileZeitung(Zeitung zeitung)
        {
            foreach (Abonnent abonnent in abonnentenList)
            {
                abonnent.erhalteZeitung(zeitung);
            }
        }
    }

    public class Zeitung
    {

        //Ein examplarisches Field. 
        private String titel;

        public Zeitung(String titel)
        {
            this.titel = titel;
        }

        public String getTitel()
        {
            return titel;
        }
    }
    public interface Abonnent
    {

        void erhalteZeitung(Zeitung zeitung);

    }

    class FamilieFischer : Abonnent
    {

        public void erhalteZeitung(Zeitung zeitung)
        {
            Console.WriteLine("Familie Fischer erhielt die aktuelle Zeitung: " + zeitung.getTitel());
        }
    }

    class FamilieMeier : Abonnent
    {

        public void erhalteZeitung(Zeitung zeitung)
        {
            Console.WriteLine("Familie Meier erhielt die aktuelle Zeitung: " + zeitung.getTitel());
        }
    }

    class FirmaXY : Abonnent
    {

        public void erhalteZeitung(Zeitung zeitung)
        {
            Console.WriteLine("Firma XY erhielt die aktuelle Zeitung: " + zeitung.getTitel());
        }
    }
}
