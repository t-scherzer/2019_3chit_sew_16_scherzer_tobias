﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200601_TimeToolCSVLogic
{
    public class AllTasks
    {
        string argsPath;
        public ObservableCollection<Task> NotImportantNotUrgent
        {
            get; set;
        }
        public ObservableCollection<Task> NotImportantUrgent
        {
            get; set;
        }
        public ObservableCollection<Task> ImportantNotUrgent
        {
            get; set;
        }
        public ObservableCollection<Task> ImportantUrgent
        {
            get; set;
        }
        public void AddTask(Task task, bool urgent, bool important)
        {
            if (important)
            {
                if (urgent) { ImportantUrgent.Add(task); Write("IU.csv", ImportantUrgent); }
                else { ImportantNotUrgent.Add(task); Write("INU.csv", ImportantNotUrgent); }
            }
            else
            {
                if (urgent) { NotImportantUrgent.Add(task); Write("NIU.csv", NotImportantUrgent); }
                else { NotImportantNotUrgent.Add(task); Write("NINU.csv", NotImportantNotUrgent); }
            }
        }
        public void SaveTasks()
        {
            Write("NINU.csv", NotImportantNotUrgent);
            Write("NIU.csv", NotImportantUrgent);
            Write("INU.csv", ImportantNotUrgent);
            Write("IU.csv", ImportantUrgent);
        }
        private void Write(string path, ObservableCollection<Task> tasks)
        {
            FileStream fs = new FileStream((argsPath == null) ? path : argsPath, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            foreach (Task t in tasks)
            {
                sw.WriteLine(t.ToCSV());
            }
            sw.Close();
            fs.Close();
        }
        public void DeleteTask(bool important, bool urgent, int id)
        {
            if (important)
            {
                if (urgent) { ImportantUrgent.RemoveAt(id); }
                else { ImportantNotUrgent.RemoveAt(id); }
            }
            else
            {
                if (urgent) { NotImportantUrgent.RemoveAt(id); }
                else { NotImportantNotUrgent.RemoveAt(id); }
            }
            SaveTasks();
        }
        public void DeleteTask(bool important, bool urgent, Task task)
        {
            if (important)
            {
                if (urgent) { ImportantUrgent.Remove(task); }
                else { ImportantNotUrgent.Remove(task); }
            }
            else
            {
                if (urgent) { NotImportantUrgent.Remove(task); }
                else { NotImportantNotUrgent.Remove(task); }
            }
            SaveTasks();
        }
        public void EditTask(Task task, bool important, bool urgent, int id)
        {
            if (important)
            {
                if (urgent) { ImportantUrgent[id] = task; }
                else { ImportantNotUrgent[id] = task; }
            }
            else
            {
                if (urgent) { NotImportantUrgent[id] = task; }
                else { NotImportantNotUrgent[id] = task; }
            }
            SaveTasks();
        }
        public Task FindTask(string name, bool important, bool urgent)
        {
            if (important)
            {
                if (urgent) { return ImportantUrgent.First(x => x.Name == name); }
                else { return ImportantNotUrgent.First(x => x.Name == name); }
            }
            else
            {
                if (urgent) { return NotImportantUrgent.First(x => x.Name == name); }
                else { return NotImportantNotUrgent.First(x => x.Name == name); }
            }
        }
        private ObservableCollection<Task> Read(string path)
        {
            ObservableCollection<Task> coll = new ObservableCollection<Task>();
            FileStream fs = new FileStream((argsPath == null)? path : argsPath, FileMode.Open);
            StreamReader sr = new StreamReader(fs);
            StringBuilder sb = new StringBuilder("");
            string[] split;
            while(sr.Peek() > -1)
            {
                sb.Clear();
                sb.Append(sr.ReadLine());
                split = sb.ToString().Split(';');
                coll.Add(new Task(split[0], split[1], Convert.ToBoolean(split[2]), Convert.ToBoolean(split[3]), (EState)Convert.ToInt32(split[4]), Convert.ToDateTime(split[5])));
            }
            sr.Close();
            fs.Close();
            return coll;
        }
        public AllTasks()
        {
            NotImportantNotUrgent = Read("NINU.csv");
            NotImportantUrgent = Read("NIU.csv");
            ImportantNotUrgent = Read("INU.csv");
            ImportantUrgent = Read("IU.csv");
        }
        public AllTasks(string argsPath) : this()
        {
            this.argsPath = argsPath;
        }
    }
}
