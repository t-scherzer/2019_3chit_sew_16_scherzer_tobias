﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200601_TimeToolCSVLogic
{
    public enum EState { OPEN, INPROGRESS, DONE}
    public class Task
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Urgent { get; set; }
        public bool Important { get; set; }
        public EState State { get; set; }
        public DateTime DoneTill { get; set; }
        public string ToCSV()
        {
            return $"{Name};{Description};{Urgent};{Important};{(int)State};{DoneTill}";
        }
        public Task(string name, string description, bool urgent, bool important, EState state, DateTime doneTill)
        {
            Name = name;
            Description = description;
            Urgent = urgent;
            Important = important;
            State = state;
            DoneTill = doneTill;
        }
        public override string ToString()
        {
            return $"{Name}, {Description}, {State}, {DoneTill}";
        }
    }
}
