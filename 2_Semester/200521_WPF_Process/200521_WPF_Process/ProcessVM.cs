﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200521_WPF_Process
{
    public class ProcessVM : INotifyPropertyChanged
    {
        public enum EState { RUNNING, READY, BLOCKED}
        private Process process = new Process();
        public event PropertyChangedEventHandler PropertyChanged;
        public Process GetChild() { return process; }
        public void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public int Id
        {
            get { return process.Id; }
            set { process.Id = value; try { OnPropertyChanged("Id"); } catch (Exception e) { } }
        }
        public int Importance
        {
            get { return process.Importance; }
            set { process.Importance = value; try { OnPropertyChanged("Importance"); } catch (Exception e) { } }
        }
        public bool Sheduled
        {
            get { return process.Sheduled; }
            set { process.Sheduled = value; try { OnPropertyChanged("Sheduled"); } catch (Exception e) { } }
        }
        public bool ThreadSafe
        {
            get { return process.ThreadSafe; }
            set { process.ThreadSafe = value; try { OnPropertyChanged("ThreadSafe"); } catch (Exception e) { } }
        }
        public string Description
        {
            get { return process.Description; }
            set { process.Description = value; try { OnPropertyChanged("Description"); } catch (Exception e) { } }
        }
        public int ProgressPercent
        {
            get { return process.ProgressPercent; }
            set { process.ProgressPercent = value; try { OnPropertyChanged("ProgressPercent"); } catch (Exception e) { } }
        }
        public EState State
        {
            get { return process.State; }
            set { process.State = value; try { OnPropertyChanged("State"); } catch (Exception e) { } }
        }
    }
}
