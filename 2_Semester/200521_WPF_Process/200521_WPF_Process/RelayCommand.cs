﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200521_WPF_Process
{
    class RelayCommand : ICommand
    {
        private Func<Boolean> _canExecute;
        private Action _execute;
        public event EventHandler CanExecuteChanged;
        public RelayCommand(Action execute) : this(execute, null) { }
        public RelayCommand(Action execute, Func<Boolean> canExecute)
        {
            if (execute == null) throw new ArgumentNullException("execute");
            _execute = execute;
            _canExecute = canExecute;
        }
        public Boolean CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute();
        }
        public void Execute(object parameter)
        {
            _execute();
        }
    }
}
