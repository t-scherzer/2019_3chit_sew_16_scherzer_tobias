﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200521_WPF_Process
{
    public class Process
    {
        //public enum EState { RUNNING, BLOCKED, READY}
        private int id;
        //Value [1-10]
        private int importance;
        private bool sheduled;
        private string description;
        private bool threadSafe;
        public int progressPercent;
        private ProcessVM.EState state;
        public int Id { get => id; set => id = value; }
        public int Importance { get => importance; set => importance = value; }
        public bool Sheduled { get => sheduled; set => sheduled = value; }
        public bool ThreadSafe { get => threadSafe; set => threadSafe = value; }
        public string Description { get => description; set => description = value; }
        public int ProgressPercent { get => progressPercent; set => progressPercent = value; }
        public ProcessVM.EState State { get => state; set => state = value; }
        public Process() { }
        public Process(int id, int importance, bool sheduled,
        bool threadsafe, string desc, int progress, ProcessVM.EState state)
        {
            this.id = id;
            this.importance = importance;
            this.sheduled = sheduled;
            this.threadSafe = threadsafe;
            this.description = desc;
            this.progressPercent = progress;
            this.state = state;
        }
        public string ToCSV()
        {
            return $"{Id};{Importance};{Sheduled};{ThreadSafe};{Description};{ProgressPercent};{State}";
        }
        public void WriteProcess()
        {
            FileStream fs = new FileStream("processes.csv", FileMode.Append);
            StreamWriter sr = new StreamWriter(fs);
            sr.WriteLine(ToCSV());
            sr.Close();
            fs.Close();
        }
        public void RemoveProcess()
        {
            StringBuilder search = new StringBuilder($"{Id};{Importance};{Sheduled};{ThreadSafe};{Description};{ProgressPercent};{State}");
            FileStream fs = new FileStream("processes.csv", FileMode.Open);
            StreamReader sr = new StreamReader(fs);
            StringBuilder fullString = new StringBuilder(sr.ReadToEnd());
            fullString.Replace(search.ToString(), "");
            sr.Close();
            fs = new FileStream("processes.csv", FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            sw.Write(fullString);
            sw.Close();
            fs.Close();
        }
        public static List<Process> Read()
        {
            List<Process> processes = new List<Process>();
            StreamReader sr = new StreamReader("processes.csv");
            string line;
            string[] words;
            while((line = sr.ReadLine()) != null)
            {
                words = line.Split(';');
                processes.Add(new Process(Convert.ToInt32(words[0]), Convert.ToInt32(words[1]), Convert.ToBoolean(words[2]), Convert.ToBoolean(words[3]), words[4], Convert.ToInt32(words[5]), (ProcessVM.EState)Enum.Parse(typeof(ProcessVM.EState), words[6])));
            }
            sr.Close();
            return processes;
        }
    }
}
