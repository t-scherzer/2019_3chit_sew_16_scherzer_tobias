﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200521_WPF_Process
{
    class ProcessesVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public ObservableCollection<ProcessVM> Processes { get; private set; } = new ObservableCollection<ProcessVM>();
        private void AddCommand() { CurrentProcess.GetChild().WriteProcess(); Processes.Add(CurrentProcess); SelectedItem = null; }
        private void ClearCommand() { SelectedItem = null; }
        private void DeleteCommand() { CurrentProcess.GetChild().RemoveProcess(); Processes.Remove(CurrentProcess); }
        public RelayCommand AddCommandRelay
            => new RelayCommand(AddCommand);
        public RelayCommand ClearCommandRelay
            => new RelayCommand(ClearCommand);
        public RelayCommand DeleteCommandRelay
            => new RelayCommand(DeleteCommand);
        public ProcessVM SelectedItem
        {
            get => Processes.Contains(CurrentProcess) ? CurrentProcess : null;
            set => CurrentProcess = value == null ? new ProcessVM() : value;
        }
        private ProcessVM currentProcess = new ProcessVM();
        public ProcessVM CurrentProcess
        {
            get { return currentProcess; }
            set { currentProcess = value; OnPropertyChanged("CurrentProcess"); }
        }
        public ProcessesVM()
        {
            List<Process> list = Process.Read();
            foreach(Process process in list)
            {
                Processes.Add(new ProcessVM { Id = process.Id, Description = process.Description, Importance = process.Importance, ProgressPercent = process.ProgressPercent, Sheduled = process.Sheduled, State = process.State, ThreadSafe = process.ThreadSafe });
            }
        }
    }
}
