﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Foabn
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ChangeColor(byte red, byte green, byte blue)
        {
            this.Resources["Color"] = new SolidColorBrush(Color.FromRgb(red, green, blue));
        }

        private void ColorRed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ChangeColor((byte)ColorRed.Value, (byte)ColorGreen.Value, (byte)ColorBlue.Value);
            this.Resources["ColorRed"] = new SolidColorBrush(Color.FromRgb((byte)ColorRed.Value, 0, 0));
        }

        private void ColorGreen_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ChangeColor((byte)ColorRed.Value, (byte)ColorGreen.Value, (byte)ColorBlue.Value);
            this.Resources["ColorGreen"] = new SolidColorBrush(Color.FromRgb(0, (byte)ColorGreen.Value, 0));
        }

        private void ColorBlue_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ChangeColor((byte)ColorRed.Value, (byte)ColorGreen.Value, (byte)ColorBlue.Value);
            this.Resources["ColorBlue"] = new SolidColorBrush(Color.FromRgb(0, 0, (byte)ColorBlue.Value));
        }
    }
}
