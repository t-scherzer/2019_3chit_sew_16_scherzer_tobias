﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lambda
{
    public delegate void Print(int value);
    class Program
    {
        public static void PrintHelperMethod(Print printDel, int val)
        {
            val *= 3;
            printDel(val);
        }
        static void Main(string[] args)
        {
            //PrintHelperMethod(delegate (int val) { Console.WriteLine("Unnedig komplizierte Methode: " + val); }, 3);
            //TestFunc();
            //Action<int> action = WriteStuff;
            //action(3);

            Func<int, int> func = i => i * 3;
            Action<int> print = i => Console.WriteLine(i);
            Predicate<string> makeBig = s => s.ToUpper() == s;
            Console.WriteLine(func(2));
            print(2);
            Console.WriteLine(makeBig("sauce"));
            Console.ReadLine();
        }
        public static void WriteStuff(int i) { Console.WriteLine(i); }
        public static void TestFunc()
        {
            Func<int, int> times3;
            times3 = delegate (int i) { Console.WriteLine("Mal 3 = " + i*3); return i * 3; };
            times3(2);
            
        }
    }

    
}
