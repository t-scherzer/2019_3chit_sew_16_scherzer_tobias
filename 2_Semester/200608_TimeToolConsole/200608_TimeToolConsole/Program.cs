﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200608_TimeToolConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                View view = new View();
                view.WriteAllTasks(view.controller.allTasks);
                Console.WriteLine("Befehl eingeben: ");
                string command = Console.ReadLine();
                try { view.Action(command, args[0]); }
                catch(Exception e) { view.Action(command, null); }
                
            }
        }
    }
}
