﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200608_TimeToolConsole
{
    class View
    {
        public Controller controller = new Controller();
        public void WriteTask(_200601_TimeToolCSVLogic.Task t)
        {
            Console.WriteLine($"Name: {t.Name}, Beschreibung: {t.Description}, Status: {t.State.ToString()}, Fertig: {t.DoneTill.ToString()}");
        }
        public void WriteAllTasks(_200601_TimeToolCSVLogic.AllTasks allTasks)
        {
            Console.WriteLine("Wichtig/ Dringend:");
            for (int i = 0; i < allTasks.ImportantUrgent.Count; i++)
            {
                WriteTask(allTasks.ImportantUrgent[i]);
                Console.WriteLine("ID: " + i);
            }
            Console.WriteLine("Wichtig/ Nicht Dringend:");
            for (int i = 0; i < allTasks.ImportantNotUrgent.Count; i++)
            {
                WriteTask(allTasks.ImportantNotUrgent[i]);
                Console.WriteLine("ID: " + i);
            }
            Console.WriteLine("Nicht Wichtig/ Dringend:");
            for (int i = 0; i < allTasks.NotImportantUrgent.Count; i++)
            {
                WriteTask(allTasks.NotImportantUrgent[i]);
                Console.WriteLine("ID: " + i);
            }
            Console.WriteLine("Nicht Wichtig/ Nicht Dringend:");
            for (int i = 0; i < allTasks.NotImportantNotUrgent.Count; i++)
            {
                WriteTask(allTasks.NotImportantNotUrgent[i]);
                Console.WriteLine("ID: " + i);
            }
        }
        public void Action(string action, string path)
        {
            switch(action.ToLower())
            {
                case "add":
                    AddTask();
                    break;
                case "edit":
                    EditTask();
                    break;
                case "delete":
                    DeleteTask();
                    break;
                case "end":
                    Environment.Exit(0);
                    break;
            }
        }
        public void DeleteTask()
        {
            bool important, urgent;
            int id;
            FindTask(out important, out urgent, out id);
            controller.allTasks.DeleteTask(important, urgent, id);
        }
        public void EditTask()
        {
            bool important, urgent;
            int id;
            FindTask(out important, out urgent, out id);
            controller.allTasks.EditTask(MakeTask(), important, urgent, id);
        }
        private void FindTask(out bool important, out bool urgent, out int id)
        {
            Console.WriteLine("Id: ");
            id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Wichtig: ");
            important = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Dringend: ");
            urgent = Convert.ToBoolean(Console.ReadLine());
        }
        public void AddTask()
        {
            _200601_TimeToolCSVLogic.Task task = MakeTask();
            controller.allTasks.AddTask(task, task.Urgent, task.Important);
        }
        private _200601_TimeToolCSVLogic.Task MakeTask()
        {
            bool important, urgent;
            string name, description;
            DateTime dateTime;
            _200601_TimeToolCSVLogic.EState state;
            Console.WriteLine("Name: ");
            name = Console.ReadLine();
            Console.WriteLine("Beschreibung: ");
            description = Console.ReadLine();
            Console.WriteLine("Datum: ");
            dateTime = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Wichtig: ");
            important = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Dringend: ");
            urgent = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Status: ");
            state = (_200601_TimeToolCSVLogic.EState)(Convert.ToInt32(Console.ReadLine()));
            return new _200601_TimeToolCSVLogic.Task(name, description, urgent, important, state, dateTime);
        }
    }
}
