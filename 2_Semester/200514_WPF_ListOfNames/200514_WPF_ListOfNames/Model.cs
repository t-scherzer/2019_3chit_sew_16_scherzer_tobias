﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200514_WPF_ListOfNames
{
    class Model : INotifyPropertyChanged
    {
        public string CurrentName
        {
            get { return mCurrentName; }
            set
            {
                mCurrentName = value;
                OnPropertyChanged("CurrentName");
            }
        }
        string mCurrentName;
        public ObservableCollection<string> AddedNames { get; } = new ObservableCollection<string>();
        public ICommand AddCommand { get; private set; }
        public Model()
        {
            AddCommand = new AddNameCommand(this);
        }
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
