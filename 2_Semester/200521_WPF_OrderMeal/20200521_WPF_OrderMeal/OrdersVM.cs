﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20200521_WPF_OrderMeal
{
    class OrdersVM : INotifyPropertyChanged
    {
        private Orders orders = new Orders();
        public Order CurrentOrder
        {
            get { return orders._Orders[CurrDay]; }
            set { orders._Orders[CurrDay] = value; }
        }
        private int currDay = 0;
        public int CurrDay
        {
            get { return currDay; }
            set { currDay = value; CurrentOrder = orders._Orders[CurrDay]; OnPropertyChanged("CurrentOrder"); OnPropertyChanged("CurrDay"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
