﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20200521_WPF_OrderMeal
{
    class Order
    {
        public bool Breakfast { get; set; }
        public bool Lunch { get; set; }
        public bool Dinner { get; set; }
        public enum Day { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday}
        public Day OrderDay { get; set; }
        
    }
}
