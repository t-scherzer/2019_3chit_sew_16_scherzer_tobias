﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20200521_WPF_OrderMeal
{
    class Orders
    {
        ObservableCollection<Order> orders = new ObservableCollection<Order>();
        public ObservableCollection<Order> _Orders
        {
            get { return orders; }
            set { orders = value; }
        }
        public Orders()
        {
            _Orders.Add(new Order { OrderDay = Order.Day.Monday});
            _Orders.Add(new Order { OrderDay = Order.Day.Tuesday });
            _Orders.Add(new Order { OrderDay = Order.Day.Wednesday });
            _Orders.Add(new Order { OrderDay = Order.Day.Thursday });
            _Orders.Add(new Order { OrderDay = Order.Day.Friday });
            _Orders.Add(new Order { OrderDay = Order.Day.Saturday });
            _Orders.Add(new Order { OrderDay = Order.Day.Sunday });
        }
    }
}
