﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200507_WPF_Student_Subject_List
{
    class SubjectViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Subject> SubjectCollection { get; private set; } = new ObservableCollection<Subject>();
        private Subject currenbSubject = new Subject();
        public string Path { get; set; } = "Subjects.csv";
        public ICommand AddCommand { get; private set; }
        public ICommand LoadCommand { get; private set; }
        public SubjectViewModel()
        {
            AddCommand = new AddCommand(this);
            LoadCommand = new LoadCommand(this);
        }
        public string CurrentCode
        {
            get { return currenbSubject.Code; }
            set { currenbSubject.Code = value; }

        }
        public string CurrentName
        {
            get { return currenbSubject.Name; }
            set { currenbSubject.Name = value; }
        }
        public bool IsFavourite
        {
            get { return currenbSubject.IsFavourite; }
            set { currenbSubject.IsFavourite = value; }
        }
        public int CurrentClazz
        {
            get { return (int)currenbSubject.Difficulty; }
            set { currenbSubject.Difficulty = (EDifficulty)value; }
        }
        public Subject CurrentSubject
        {
            get { return currenbSubject; }
            set { currenbSubject = value; }
        }
        private void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public void SaveInCSV()
        {
            StreamWriter sw = new StreamWriter(Path, true);
            sw.WriteLine(currenbSubject.ToCSV());
            sw.Close();
        }
        public void LoadFromCSV()
        {
            SubjectCollection.Clear();
            StreamReader sr = new StreamReader(Path, true);
            while (sr.Peek() > -1)
            {
                currenbSubject.LoadFromCSVLine(sr.ReadLine());
                SubjectCollection.Add(currenbSubject);
            }
            sr.Close();
        }
    }
}
