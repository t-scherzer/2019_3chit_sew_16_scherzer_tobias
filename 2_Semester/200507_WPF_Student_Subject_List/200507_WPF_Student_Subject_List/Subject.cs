﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200507_WPF_Student_Subject_List
{
    public enum EDifficulty { Easy, Moderate, Hard}
    class Subject
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsFavourite { get; set; }
        public EDifficulty Difficulty { get; set; }
        public Subject(string code, string name, bool fav, EDifficulty difficulty)
        {
            Code = code;
            Name = name;
            IsFavourite = fav;
            Difficulty = difficulty;
        }
        public Subject() { }
        public string ToCSV()
        {
            return Code + ";" + Name + ";" + IsFavourite + ";" + (int)Difficulty;
        }
        public void LoadFromCSVLine(string line)
        {
            string[] arr = line.Split(';');
            Code = arr[0];
            Name = arr[1];
            IsFavourite = Convert.ToBoolean(arr[2]);
            Difficulty = (EDifficulty)(Convert.ToInt32(arr[3]));
        }
    }
}
