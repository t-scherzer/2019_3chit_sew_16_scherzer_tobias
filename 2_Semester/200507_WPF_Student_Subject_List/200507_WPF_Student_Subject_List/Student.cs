﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200507_WPF_Student_Subject_List
{
    public enum EClass { _1CHIT, _2CHIT, _3CHIT, _4CHIT, _5CHIT}
    class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; } = 0;
        public EClass Clazz { get; set; }
        public Student(string firstname, string lastname, int age, EClass clazz)
        {
            FirstName = firstname;
            LastName = lastname;
            Age = age;
            Clazz = clazz;
        }
        public Student() { }
        public string ToCSV()
        {
            return FirstName + ";" + LastName + ";" + Age + ";" + (int)Clazz;
        }
        public void LoadFromCSVLine(string line)
        {
            string[] arr = line.Split(';');
            FirstName = arr[0];
            LastName = arr[1];
            Age = Convert.ToInt32(arr[2]);
            Clazz = (EClass)(Convert.ToInt32(arr[3]));
        }
    }
}
