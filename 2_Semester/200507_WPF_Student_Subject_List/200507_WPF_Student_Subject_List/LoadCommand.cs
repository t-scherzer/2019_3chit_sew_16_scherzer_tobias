﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200507_WPF_Student_Subject_List
{
    class LoadCommand : ICommand
    {
        StudentViewModel parentstu;
        SubjectViewModel parentsub;
        public event EventHandler CanExecuteChanged;
        public LoadCommand(StudentViewModel parent)
        {
            this.parentstu = parent;
        }
        public LoadCommand(SubjectViewModel parent)
        {
            this.parentsub = parent;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            if (parentstu != null)
            {
                parentstu.LoadFromCSV();
            }
            else if (parentsub != null)
            {
                parentsub.LoadFromCSV();
            }
        }
    }
}
