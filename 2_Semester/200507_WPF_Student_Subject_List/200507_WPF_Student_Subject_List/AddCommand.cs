﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace _200507_WPF_Student_Subject_List
{
    class AddCommand : ICommand
    {
        StudentViewModel parentstu;
        SubjectViewModel parentsub;
        public event EventHandler CanExecuteChanged;
        public AddCommand(StudentViewModel parent)
        {
            this.parentstu = parent;
        }
        public AddCommand(SubjectViewModel parent)
        {
            this.parentsub = parent;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            if(parentstu != null)
            {
                Student student = new Student();
                Grid parent = ((parameter as Button).Parent as Grid);
                student.FirstName = (parent.FindName("firstName") as TextBox).Text;
                student.LastName = (parent.FindName("lastName") as TextBox).Text;
                student.Age = Convert.ToInt32((parent.FindName("age") as TextBox).Text);
                student.Clazz = (EClass)Convert.ToInt32((parent.FindName("clazz") as ComboBox).SelectedIndex);

                parentstu.StudentCollection.Add(student);
                parentstu.CurrentStudent = student;
                parentstu.SaveInCSV();
            }
            else if(parentsub != null)
            {
                Subject subject = new Subject();
                Grid parent = ((parameter as Button).Parent as Grid);
                subject.Code = (parent.FindName("code") as TextBox).Text;
                subject.Name = (parent.FindName("name") as TextBox).Text;
                subject.IsFavourite = Convert.ToBoolean((parent.FindName("isFavourite") as CheckBox).IsChecked);
                if(Convert.ToBoolean((parent.FindName("easyCheck") as RadioButton).IsChecked))
                {
                    subject.Difficulty = EDifficulty.Easy;
                }
                else if (Convert.ToBoolean((parent.FindName("moderateCheck") as RadioButton).IsChecked))
                {
                    subject.Difficulty = EDifficulty.Moderate;
                }
                else
                {
                    subject.Difficulty = EDifficulty.Hard;
                }
                parentsub.SubjectCollection.Add(subject);
                parentsub.CurrentSubject = subject;
                parentsub.SaveInCSV();
            }
        }
    }
}
