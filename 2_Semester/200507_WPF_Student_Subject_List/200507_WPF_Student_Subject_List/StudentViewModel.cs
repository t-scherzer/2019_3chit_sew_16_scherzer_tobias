﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace _200507_WPF_Student_Subject_List
{
    class StudentViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Student> StudentCollection { get; private set; } = new ObservableCollection<Student>();
        private Student currentStudent = new Student();
        public string Path { get; set; } = "Students.csv";
        public ICommand AddCommand { get; private set; }
        public ICommand LoadCommand { get; private set; }
        public StudentViewModel()
        {
            AddCommand = new AddCommand(this);
            LoadCommand = new LoadCommand(this);
        }
        public string CurrentFirstName
        {
            get { return currentStudent.FirstName; }
            set { currentStudent.FirstName = value; }
            
        }
        public string CurrentLastName
        {
            get { return currentStudent.LastName; }
            set { currentStudent.LastName = value; }
        }
        public int CurrentAge
        {
            get { return currentStudent.Age; }
            set { currentStudent.Age = value; }
        }
        public int CurrentClazz
        {
            get { return (int)currentStudent.Clazz; }
            set { currentStudent.Clazz = (EClass)value; }
        }
        public Student CurrentStudent
        {
            get { return currentStudent; }
            set { currentStudent = value; }
        }
        private void OnPropertyChanged(string property)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        public void SaveInCSV()
        {
            StreamWriter sw = new StreamWriter(Path, true);
            sw.WriteLine(currentStudent.ToCSV());
            sw.Close();
        }
        public void LoadFromCSV()
        {
            StudentCollection.Clear();
            StreamReader sr = new StreamReader(Path, true);
            while(sr.Peek() > -1)
            {
                currentStudent.LoadFromCSVLine(sr.ReadLine());
                StudentCollection.Add(currentStudent);
            }
            sr.Close();
        }
    }
}
