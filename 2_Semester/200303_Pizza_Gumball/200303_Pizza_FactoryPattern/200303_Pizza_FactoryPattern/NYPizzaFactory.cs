﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Pizza_FactoryPattern
{
    class NYPizzaFactory : PizzaStore
    {
        public override Pizza CreatePizza(string type)
        {
            Pizza pizza = OrderPizza(type);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }
        public override Pizza OrderPizza(string type)
        {
            Pizza pizza;
            switch (type)
            {
                case "clam":
                    pizza = new NYClamPizza();
                    break;
                case "cheese":
                    pizza = new NYCheesePizza();
                    break;
                case "veggie":
                    pizza = new NYVeggiePizza();
                    break;
                case "pepperoni":
                    pizza = new NYPepperoniPizza();
                    break;
                default:
                    pizza = new NYCheesePizza();
                    break;
            }
            return pizza;
        }
    }
}
