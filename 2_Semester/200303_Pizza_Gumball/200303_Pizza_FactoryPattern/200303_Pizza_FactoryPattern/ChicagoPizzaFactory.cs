﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Pizza_FactoryPattern
{
    class ChicagoPizzaFactory : PizzaStore
    {
        public override Pizza CreatePizza(string type)
        {
            Pizza pizza = OrderPizza(type);
            pizza.Prepare();
            pizza.Bake();
            pizza.Cut();
            pizza.Box();
            return pizza;
        }
        public override Pizza OrderPizza(string type)
        {
            Pizza pizza;
            switch (type)
            {
                case "clam":
                    pizza = new ChicagoClamPizza();
                    break;
                case "cheese":
                    pizza = new ChicagoCheesePizza();
                    break;
                case "veggie":
                    pizza = new ChicagoVeggiePizza();
                    break;
                case "pepperoni":
                    pizza = new ChicagoPepperoniPizza();
                    break;
                default:
                    pizza = new ChicagoCheesePizza();
                    break;
            }
            return pizza;
        }
    }
}
