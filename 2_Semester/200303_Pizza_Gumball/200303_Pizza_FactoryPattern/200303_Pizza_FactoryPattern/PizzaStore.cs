﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Pizza_FactoryPattern
{
    abstract class PizzaStore
    {
        abstract public Pizza CreatePizza(string type);
        abstract public Pizza OrderPizza(string type);
    }
}
