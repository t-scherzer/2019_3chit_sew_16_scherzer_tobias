﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Pizza_FactoryPattern
{
    class SimplePizzaFactory
    {
        Pizza OrderPizza(string type)
        {
            Pizza pizza;
            switch (type)
            {
                case "clam":
                    pizza = new ClamPizza();
                    break;
                case "cheese":
                    pizza = new CheesePizza();
                    break;
                case "veggie":
                    pizza = new VeggiePizza();
                    break;
                case "pepperoni":
                    pizza = new PepperoniPizza();
                    break;
                default:
                    pizza = new CheesePizza();
                    break;
            }
            
            return pizza;
        }
    }
}
