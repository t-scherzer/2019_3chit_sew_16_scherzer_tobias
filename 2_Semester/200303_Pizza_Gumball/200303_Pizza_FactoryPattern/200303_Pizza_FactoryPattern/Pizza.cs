﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Pizza_FactoryPattern
{
    class Pizza
    {
        public void Bake() { Console.WriteLine("Pizza is baked"); }
        public void Cut() { Console.WriteLine("Pizza is cut"); }
        public void Prepare() { Console.WriteLine("Pizza is prepared"); }
        public void Box() { Console.WriteLine("Pizza is boxed"); }
    }
}
