﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Gumball_State
{
    class NoQuarter : Gumball_State, IGumballState
    {
        public void TurnCrank()
        {
            Console.WriteLine("Noch kein Quarter drin");
        }
        public void InsertQuarter()
        {
            base.state = new HasQuarter();
        }
        public void EjectQuarter()
        {
            Console.WriteLine("Wenn kein Geld drin ist, kommt auch nix raus. VO NIX KUMMT NIX");
        }
        public void DispenseGumball()
        {
            Console.WriteLine("Noch kein Geld erhalten");
        }
    }
}
