﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Gumball_State
{
    interface IGumballState
    {
        void TurnCrank();
        void InsertQuarter();
        void EjectQuarter();
        void DispenseGumball();
    }
}
