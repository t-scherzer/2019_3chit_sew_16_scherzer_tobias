﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Gumball_State
{
    class NoGumballs : Gumball_State, IGumballState
    {
        public void TurnCrank()
        {
            Console.WriteLine("Schade, kein Gumball HAHAHAHA");
        }
        public void InsertQuarter()
        {
            Console.WriteLine("Wenn der LKW nicht fahren kann, gibt es auch keine Gumballs. Unterstütze deshalb LKW Friends on the Road ;)");
        }
        public void EjectQuarter()
        {
            Console.WriteLine("Wenn kein Geld drin ist, kommt auch nix raus. VO NIX KUMMT NIX");
        }
        public void DispenseGumball()
        {
            Console.WriteLine("Schade, gibts ned");
        }
    }
}
