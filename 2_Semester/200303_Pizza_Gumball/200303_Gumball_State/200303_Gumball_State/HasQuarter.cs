﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Gumball_State
{
    class HasQuarter : Gumball_State, IGumballState
    {
        public void TurnCrank()
        {
            base.state = new GumballSold();
        }
        public void InsertQuarter()
        {
            Console.WriteLine("Zuerst Gumball entnehmen");
        }
        public void EjectQuarter()
        {
            base.state = new NoQuarter();
        }
        public void DispenseGumball()
        {
            Console.WriteLine("Dreh den Crank");
        }
    }
}
