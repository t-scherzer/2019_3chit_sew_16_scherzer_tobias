﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _200303_Gumball_State
{
    class GumballSold : Gumball_State, IGumballState
    {
        public void TurnCrank()
        {
            Console.WriteLine("Zuerst Gumball entnehmen");
        }
        public void InsertQuarter()
        {
            Console.WriteLine("Zuerst Gumball entnehmen");
        }
        public void EjectQuarter()
        {
            Console.WriteLine("Kein Quarter drin");
        }
        public void DispenseGumball()
        {
            Console.WriteLine("Here you go");
            if (base.amountGumballs == 0) base.state = new NoGumballs();
            else base.state = new NoQuarter();
        }
    }
}
